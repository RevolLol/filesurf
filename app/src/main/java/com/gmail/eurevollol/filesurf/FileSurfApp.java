package com.gmail.eurevollol.filesurf;

import android.app.Application;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatDelegate;

public class FileSurfApp extends Application {
    public Entries fileHandler = null;
    private SharedPreferences sharedPref;

    public void onCreate() {
        super.onCreate();
        sharedPref = getSharedPreferences("FS_set", MODE_PRIVATE);
        setTheme();
    }

    public SharedPreferences getSharedPref() {
        return sharedPref;
    }

    public void setTheme(){
        boolean useDark = sharedPref.getBoolean("theme", false),
                followSystem = sharedPref.getBoolean("theme_global", false);
        int mode = AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM;
        if(!followSystem){
            mode = useDark ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO;
        }
        AppCompatDelegate.setDefaultNightMode(mode);
    }
}
