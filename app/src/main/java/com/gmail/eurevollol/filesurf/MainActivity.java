package com.gmail.eurevollol.filesurf;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;

import java.io.Serializable;
import java.util.AbstractMap.SimpleEntry;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import com.gmail.eurevollol.filesurf.callbacks.dlgActionCallback;
import com.gordonwong.materialsheetfab.MaterialSheetFab;

import org.json.JSONException;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AccessesApp {
    public static final byte del = 1, copy = 2, rename =  4, add = 8, add_dir=9, paste = 16, share = 32, zip = 33; //cut is copy + del

    private RecyclerView rv;
    private bmManager bookmarks;
    private MaterialSheetFab materialSheetFab;
    private Stack<SimpleEntry> rv_pos = new Stack<>();//stack of recyclerview's positions
    private FileAdapter fa;
    Toolbar toolbar;
    private ArrayList<Integer> picked = new ArrayList<>();
    private byte last_action;
    private dlgActionCallback performCallback = (CustDialog dissDialog) -> {
        Map<String, Object> resultingBundle = dissDialog.getResultingBundle();
        Integer dlgActionId = (Integer) resultingBundle.get("selectedAction");
        if(dlgActionId != null && dlgActionId != -1) {
            String actionName = (String) resultingBundle.get("actionName");
            switch (dlgActionId) {
                case R.id.delete:
                    this.setLastAction(MainActivity.del);
                    this.makeConfirmDialog(actionName);
                    break;
                case R.id.copy:
                    this.setLastAction(MainActivity.copy);
                    this.performAction("");
                    break;
                case R.id.cut:
                    this.setLastAction((byte) (MainActivity.copy | MainActivity.del));
                    this.performAction("");
                    break;
                case R.id.rename:
                    this.setLastAction(MainActivity.rename);
                    this.makeConfirmDialog(actionName);
                    break;
                case R.id.share:
                    this.setLastAction(MainActivity.share);
                    this.performAction("");
                    break;
                case R.id.zip:
                    this.setLastAction(MainActivity.zip);
                    this.performAction("");
                    break;
                default:
                    this.setLastAction((byte) 0);
                    this.performAction((String)resultingBundle.get("specialAction"));
                    break;
            }
        }
        else {
            String new_name = "";
            try
            {
                new_name = ((EditText) dissDialog.getDialog().findViewById(R.id.new_name)).getText().toString();
            }
            catch (NullPointerException exp)
            {
                System.out.println(exp.getLocalizedMessage());
            }
            this.performAction(new_name);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        SharedPreferences sharedPref = this.getApp().getSharedPref();
        this.getApp().fileHandler = sharedPref.getBoolean("position",false) ? new Entries(sharedPref.getString("last_pos", "/storage/emulated/0"), getApplicationContext()) : new Entries(getApplicationContext());
        this.bookmarks = new bmManager(sharedPref.getString("bookmarks", "{}"));
        fa = new FileAdapter(this.getApp().fileHandler.getContents());
        rv = findViewById(R.id.my_recycler_view);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(mLayoutManager);
        rv.setAdapter(fa);
        setupToolbar();
        fa.setOnItemClickListener(new FileAdapter.ClickListener() {
            @Override
            public void onItemClick(final int position, View v) {
                if(fa.getSel_size()!=0 )//adds or removes clicked file to selection if selection mode is already on
                {
                    boolean[] aSelected = MainActivity.this.fa.getSelected();
                    //prevents parent directory from being added to selection
                    aSelected[position] = !aSelected[position] && position != 0;
                    controlSelected(aSelected);
                }
                else
                if (MainActivity.this.getApp().fileHandler.findByNum(position).isDirectory()) {
                    final Handler upd = new Handler(msg -> {
                        updateForNewDir(msg.getData().getString("newDir"));
                        return true;
                    });
                    Thread list = new Thread(() -> {
                        Message msg = upd.obtainMessage();
                        Bundle dataBundle = new Bundle();
                        dataBundle.putString("newDir", MainActivity.this.getApp().fileHandler.findByNum(position).getName().equals("..") ? "" : MainActivity.this.getApp().fileHandler.findByNum(position).getPath());
                        msg.setData(dataBundle);
                        upd.sendMessage(msg);
                    });
                    list.start();
                } else {
                    Intent newIntent = new Intent();
                    String absPath = MainActivity.this.getApp().fileHandler.findByNum(position).getAbsolutePath();
                    newIntent.setDataAndType(
                            MainActivity.this.getApp().fileHandler.getUrisForFiles(new String[]{absPath}).get(0),
                            MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(absPath))
                    );
                    newIntent.setAction(Intent.ACTION_VIEW)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .setFlags(android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION|android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    try {
                        startActivity(newIntent);
                    } catch (android.content.ActivityNotFoundException e) {
                        Toast.makeText(getApplicationContext(), getString(R.string.unkn_ext), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onItemLongClick(int position, View v) {
                String clickedFileName = MainActivity.this.getApp().fileHandler.findByNum(position).getName();
                if(!clickedFileName.equals("..")) {
                    //sending filename to dialog in order to get additional actions for file
                    CustDialog actionMenu = CustDialog.newInstance(
                            (ArrayList<String>) Entries.extensionActions.get(MimeTypeMap.getFileExtensionFromUrl(clickedFileName)),
                            clickedFileName,
                            performCallback
                    );
                    actionMenu.show(getSupportFragmentManager(), "action_menu");
                    picked = new ArrayList<>();
                    picked.add(position);
                }
            }
        });
        setupFab();
        rv.addOnScrollListener(new RecyclerView.OnScrollListener(){//hiding floating action button on scroll
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy != 0)
                    materialSheetFab.hideSheetThenFab();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                if (newState == RecyclerView.SCROLL_STATE_IDLE && fa.getSel_size()==0){
                    materialSheetFab.showFab();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        final SwipeRefreshLayout ref_rv = findViewById(R.id.swipeRefreshLayout);//the refreshing layout itself
        //pull down listener
        ref_rv.setOnRefreshListener(() -> {
            stop_ref(ref_rv);//and that is a dedicated function, that has to be called here, otherwise exception is being thr
        });
        setupDrawer();
    }

    @Override
    protected void onStop()
    {
        SharedPreferences.Editor ed = getSharedPreferences("FS_set", MODE_PRIVATE).edit();
        ed.putString("last_pos", this.getApp().fileHandler.getCurrDir().getPath()).apply();//writing last directory to shared pref
        super.onStop();
    }

    public FileSurfApp getApp(){
        return (FileSurfApp)getApplication();
    }

    private void setupToolbar(){
        //initial setup
        if(toolbar == null)
        {
            toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            toolbar.setTitleTextAppearance(getApplicationContext(), R.style.TextAppearance_AppCompat_Subhead_Inverse);
        }
        toolbar.setNavigationIcon(R.drawable.side_nav_bar);
        String[] titleParts = getTitleParts(this.getApp().fileHandler.getCurrPath());
        toolbar.setTitle(titleParts[0]);
        toolbar.setSubtitle(titleParts[1]);
        setupBookmarks();
    }

    //splits full current path into current directory name and path to it
    private String[] getTitleParts(String fullPath){
        int lastSlash = fullPath.lastIndexOf("/") + 1;
        return new String[]{
                fullPath.substring(lastSlash),
                getString(R.string.path) + fullPath.substring(0, lastSlash)
        };
    }

    /***
     * this method handles updates of both toolbar icons and drawer menu items
     * catches all exception itself for convenience (no need to always wrap method call in try-catch)
     */
    private void setupBookmarks(){
        try{
            boolean isBooked = this.bookmarks.isBooked(this.getApp().fileHandler.getCurrDir().getAbsolutePath());
            toolbar.getMenu().findItem(R.id.bookmarkRm).setVisible(isBooked);
            toolbar.getMenu().findItem(R.id.bookmarkAdd).setVisible(!isBooked);
            final NavigationView navigationView = findViewById(R.id.nav_view);
            new Thread(() -> {
                final HashMap<String, String> bMap = new HashMap<>(this.bookmarks.getBMap());
                navigationView.post(() -> {
                    SubMenu bmList =  navigationView.getMenu().findItem(R.id.bmList).getSubMenu();
                    bmList.clear();
                    if(bMap.isEmpty())
                    {
                        bmList.add(getString(R.string.no_bmarks)).setIcon(getResources().getDrawable(R.drawable.ic_bookmark_black_24dp, this.getTheme()));
                    }
                    else
                    {
                        for(String key : bMap.keySet())
                        {
                            MenuItem newBm = bmList.add(bMap.get(key));
                            newBm.setOnMenuItemClickListener(item -> {
                                //prevents user from stacking the same directory in history stack
                                if(!key.equals(this.getApp().fileHandler.getCurrDir().getAbsolutePath()))
                                    updateForNewDir(key);
                                return false;
                            });
                        }
                    }
                });
            }).start();
        }
        catch (NullPointerException ignored){}
    }

    /**
     * Sets up the Floating action button.
     */
    private void setupFab() {
        CustomFab fab = findViewById(R.id.fab);
        View sheetView = findViewById(R.id.fab_sheet);
        View overlay = findViewById(R.id.overlay);
        int sheetColor = getResources().getColor(R.color.background_dim_overlay);
        int fabColor = getResources().getColor(R.color.colorAccent);

        // Create material sheet FAB
        materialSheetFab = new MaterialSheetFab<>(fab, sheetView, overlay, sheetColor, fabColor);

        // Set material sheet event listener
/*        materialSheetFab.setEventListener(new MaterialSheetFabEventListener() {
            @Override
            public void onShowSheet() {
            }

            @Override
            public void onHideSheet() {
            }
        });*/

        // Set material sheet item click listeners
        findViewById(R.id.fab_sheet_item_file).setOnClickListener(v -> {
            setLastAction(MainActivity.add);
            //bitwise shift to left for 1 digit delivers us required intermediate dialog
            CustDialog interm = CustDialog.newInstance(
                    (byte)(MainActivity.this.last_action << 1),
                    getString(R.string.dummy_file_name),
                    performCallback
            );
            interm.show(getSupportFragmentManager(),getString(R.string.dummy_file_name));
        });
        findViewById(R.id.fab_sheet_item_folder).setOnClickListener(v -> {
            setLastAction(MainActivity.add);
            //bitwise shift to left for 1 digit delivers us required intermediate dialog
            CustDialog interm = CustDialog.newInstance(
                    (byte)(MainActivity.this.last_action << 1),
                    getString(R.string.dummy_folder_name),
                    performCallback
            );
            interm.show(getSupportFragmentManager(),getString(R.string.dummy_folder_name));
            setLastAction(MainActivity.add_dir);//dialog doesnt need to know about type of object user adds, so we change last action to 9(dir) after dialogs creation
        });
    }

    protected void onSaveInstanceState(Bundle outState) {//saving everything concerning Entries object, array of checkboxes statuses and stack of subdirectories entries
        super.onSaveInstanceState(outState);
        outState.putInt("scroll", ((LinearLayoutManager) this.rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition());
        outState.putBooleanArray("selected", this.fa.getSelected());
        outState.putSerializable("stack", rv_pos);
        outState.putIntegerArrayList("picked", picked);
        outState.putParcelable("entr", this.getApp().fileHandler);
    }

    protected void onRestoreInstanceState(final Bundle savedInstanceState) {//restoring current path, RecyclerView scroll position and stack of subdirectories enters
        super.onRestoreInstanceState(savedInstanceState);
        MainActivity.this.getApp().fileHandler = savedInstanceState.getParcelable("entr");
        updateForCurrDir(savedInstanceState.getInt("scroll"));
        fa.setSelected(savedInstanceState.getBooleanArray("selected"));
        Serializable tmp = savedInstanceState.getSerializable("stack");
        if(tmp!=null)//really important to check in order to avoid exception
            rv_pos = (Stack<SimpleEntry>) tmp;
        picked = savedInstanceState.getIntegerArrayList("picked");
    }

    /*does the work on pull down to refresh action and stops animation*/
    private void stop_ref(SwipeRefreshLayout l)
    {
        updateForCurrDir(0);
        if(l.isRefreshing())
            l.setRefreshing(false);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            if(this.fa.getSel_size() != 0)
                controlSelected(new boolean[MainActivity.this.getApp().fileHandler.getContents().size()]);
            else {
                if(!this.rv_pos.isEmpty() && this.getApp().fileHandler.goUpper())
                {
                    updateForNewDir("");
                }
                else {
                    super.onBackPressed();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        onCheckHandle(this.fa.getSel_size());
        if(!this.getApp().fileHandler.isQueueEmpty()) {
            menu.findItem(R.id.clear_copied).setVisible(true);
            menu.findItem(R.id.paste).setVisible(true);
        }
        return true;
    }

    private void removeBookmark(String[] dirPaths){
        for (String filePath: dirPaths)
        {
            this.bookmarks.removeBm(filePath);
        }
        this.getApp().getSharedPref().edit().putString("bookmarks", this.bookmarks.toString()).apply();
        setupBookmarks();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        setPicked();
        switch (item.getItemId())
        {
            case R.id.bookmarkAdd:
                if(this.bookmarks.getBMap().size() < 3)
                {
                    try {
                        /**
                         * hiding the menu item that just was pressed and showing an opposite one
                         * */
                        this.getApp().getSharedPref().edit().putString("bookmarks", this.bookmarks.addBm(this.getApp().fileHandler.getCurrDir().getAbsolutePath()).toString()).apply();
                        setupBookmarks();
                        Toast.makeText(this, getString(R.string.bookmarkSuccess), Toast.LENGTH_SHORT).show();
                    }
                    catch(JSONException exp){
                        Toast.makeText(this, getString(R.string.bookmarkFail), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                    Toast.makeText(this, getString(R.string.bookmarkLimit), Toast.LENGTH_LONG).show();
                break;
            case R.id.bookmarkRm:
                removeBookmark(new String[]{this.getApp().fileHandler.getCurrDir().getAbsolutePath()});
                break;
            case R.id.paste://pasting is being handled here for now, will be in fab later
                if(this.getApp().fileHandler.isQueueEmpty())
                    Toast.makeText(this, getString(R.string.empty_queue), Toast.LENGTH_SHORT).show();
                else
                    setLastAction(MainActivity.paste);
                    performAction(item.getTitle().toString());
                break;
            case R.id.mult_copy:
                setLastAction(MainActivity.copy);
                performAction(item.getTitle().toString());
                break;
            case R.id.mult_cut:
                setLastAction((byte) (MainActivity.copy | MainActivity.del));
                performAction(item.getTitle().toString());
                break;
            case R.id.mult_share:
                setLastAction(MainActivity.share);
                performAction("");
                break;
            case R.id.mult_del:
                setLastAction(MainActivity.del);
                makeConfirmDialog(this.picked.size() + getString(R.string.files));
                break;
            case R.id.multZip:
                setLastAction(MainActivity.zip);
                performAction("");
                break;
            case R.id.select_all:
                boolean[] cb = new boolean[this.getApp().fileHandler.getContents().size()];
                Arrays.fill(cb, true);
                if(this.getApp().fileHandler.goUpper())//in case we have parent directory which is not supposed to be selected
                    cb[0] = false;
                controlSelected(cb);
                break;
            case R.id.clear_copied:
                this.getApp().fileHandler.cleanQueue();
                toolbar.getMenu().findItem(R.id.clear_copied).setVisible(false);
                toolbar.getMenu().findItem(R.id.paste).setVisible(false);
                Toast.makeText(MainActivity.this, getString(R.string.copied_cleared), Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setPicked()
    {
        picked.clear();
        boolean [] sel = fa.getSelected();
        for(int j=0; j < sel.length; j++)
        {
            if(sel[j])
            {
                picked.add(j);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {//drawer menu items
        switch (item.getItemId())
        {
            case R.id.settings:
                Intent myIntent = new Intent(MainActivity.this, Settings.class);
                MainActivity.this.startActivity(myIntent);
                break;
            case R.id.switch_sd:
            case R.id.switch_external:
                updateForNewDir(this.getApp().fileHandler.getRootFor(!this.getApp().fileHandler.getIsSDCard()));
                break;
            case R.id.about:
                CustDialog about_dl = CustDialog.newInstance(CustDialog.about, BuildConfig.VERSION_NAME, performCallback);
                about_dl.show(getSupportFragmentManager(),"about_dl");
                break;
        }

        ((DrawerLayout)findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
        return true;
    }

    private void updateForNewDir(String newDir) {//update adapter on picking a different dir
        String currDir;
        int scrollPos = 0;
        if(newDir.length() > 0)//go down one level
        {
            this.rv_pos.push(new SimpleEntry<>(this.getApp().fileHandler.getCurrDir().getAbsolutePath(), ((LinearLayoutManager) this.rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition()));
            currDir = newDir;
        }
        else//go up one level
        {
            SimpleEntry tmp = this.rv_pos.size() > 0 ? this.rv_pos.pop() : new SimpleEntry<>(this.getApp().fileHandler.getCurrDir().getParent(), 0);
            currDir = tmp.getKey().toString();
            scrollPos = (int)tmp.getValue();
        }
        updateForDir(currDir, scrollPos);
        setupBookmarks();
        setupDrawer();
    }

    private void updateForCurrDir(int scroll) {//simply update adapter when directory wasn't changed
        updateForDir(this.getApp().fileHandler.getCanonPath(this.getApp().fileHandler.getCurrDir()), scroll);
        setupDrawer();
    }

    private void updateForDir(String currDir, int scrollPos){
        this.fa = (FileAdapter) this.rv.getAdapter();
        this.fa.set_list(this.getApp().fileHandler.changeDir(currDir).getContents());
        ((LinearLayoutManager) this.rv.getLayoutManager()).scrollToPositionWithOffset(scrollPos, 0);
        this.fa.setSelected(new boolean[this.getApp().fileHandler.getContents().size()]);//clearing selected files
        this.fa.notifyDataSetChanged();
        String[] titleParts = getTitleParts(this.getApp().fileHandler.getCurrPath());
        toolbar.setTitle(titleParts[0]);
        toolbar.setSubtitle(titleParts[1]);
    }

    public void onCheckHandle(byte sel_size)//toolbar, floating action button and SwipeRefresh layout
    {
        SwipeRefreshLayout sw = findViewById(R.id.swipeRefreshLayout);
        if(sel_size != 0)
        {
            //checking if we already changed title and showed menu
            if (sw.isEnabled())
            {
                sw.setEnabled(false);
                materialSheetFab.hideSheetThenFab();
                toolbar.setTitle(getString(R.string.mult_tbar_title));
                toolbar.setNavigationIcon(R.drawable.ic_clear_white_24dp);
                toolbar.setNavigationOnClickListener(v -> controlSelected(new boolean[MainActivity.this.getApp().fileHandler.getContents().size()]));
                toolbar.getMenu().setGroupVisible(R.id.mult_files, true);
            }
            toolbar.setSubtitle(sel_size + getString(R.string.files));
        } else//getting the original toolbar back and popping out the fab
        {
            materialSheetFab.showFab();
            setupToolbar();
            toolbar.getMenu().setGroupVisible(R.id.mult_files, false);
            setupDrawer();
            sw.setEnabled(true);
        }
    }

    private void updateStorageSwitch(){
        Menu navMenu = ((NavigationView)findViewById(R.id.nav_view)).getMenu();
        //checking if there is an sdcard
        final boolean hasSD = this.getApp().fileHandler.getSdCardPaths() != null;
        boolean isSDCard = this.getApp().fileHandler.getIsSDCard();
        navMenu.findItem(R.id.switch_sd).setVisible(!isSDCard);
        navMenu.findItem(R.id.switch_sd).setEnabled(hasSD);
        navMenu.findItem(R.id.switch_external).setVisible(isSDCard);
    }

    /**
     * default drawer setup method for initialization or resetting
     * */
    private void setupDrawer()
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        new Thread(() -> {
            final Object[] memTotal = new Object[]{
                    this.getApp().fileHandler.getIsSDCard() ? getString(R.string.mem_sd) : getString(R.string.mem_total),
                    MainActivity.this.fa.get_size(MainActivity.this.getApp().fileHandler.getTotalMemory())
            };
            final Object[] memInfo = new Object[]{
                MainActivity.this.fa.get_size(MainActivity.this.getApp().fileHandler.getBusyMemory()),
                getResources().getString(R.string.mem_used),
                MainActivity.this.fa.get_size(MainActivity.this.getApp().fileHandler.getFreeMemory()),
                getResources().getString(R.string.mem_free)
            };
            navigationView.post(() -> {
                View navHeaderView = navigationView.getHeaderView(0);
                if(navHeaderView == null)
                    navHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_main);
                ((TextView)navHeaderView.findViewById(R.id.mem_info)).setText(MessageFormat.format("{0} {1}, {2} {3}", memInfo));
                ((TextView)navHeaderView.findViewById(R.id.total_mem)).setText(MessageFormat.format("{0} {1}", memTotal));
                MainActivity.this.updateStorageSwitch();
            });
        }).start();
    }

    private void controlSelected(boolean [] sel_array)
    {
        this.fa.setSelected(sel_array);
        onCheckHandle(this.fa.getSel_size());
        this.fa.notifyDataSetChanged();//setting all checkboxes according to passed array
    }

    public void makeConfirmDialog(String action_name)
    {
        String temp_dialog_str = "";
        try {
            temp_dialog_str = this.getApp().fileHandler.findByNum(picked.get(0)).getName();
        }
        catch (ArrayIndexOutOfBoundsException exp)//when pasting there's an exception
        {
            System.out.println(exp.getLocalizedMessage());
        }
        if(picked.size() > 1)//if working with more than 1 file pass number of files to dialog instead of names
            temp_dialog_str = picked.size() + getString(R.string.files);
        CustDialog interm = CustDialog.newInstance((byte)(this.last_action << 1), temp_dialog_str, performCallback);
        interm.show(getSupportFragmentManager(),action_name);
    }

    public void setLastAction(byte act_id)
    {
        this.last_action = act_id;
    }

    private boolean shareFiles(String[] paths){
        boolean multFiles = picked.size() != 1;
        String mimeType = null;
        Intent intentShareFile = new Intent(multFiles ? Intent.ACTION_SEND_MULTIPLE : Intent.ACTION_SEND);
        if(multFiles)
            intentShareFile.putParcelableArrayListExtra(Intent.EXTRA_STREAM, this.getApp().fileHandler.getUrisForFiles(paths));
        else
        {
            intentShareFile.putExtra(Intent.EXTRA_STREAM, this.getApp().fileHandler.getUrisForFiles(paths).get(0));
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(MainActivity.this.getApp().fileHandler.findByNum(picked.get(0)).getAbsolutePath())
            );
        }
        //in case we couldnt define correct mime type for an unknown file extension
        mimeType = mimeType == null ? "*/*" : mimeType;
        /**
         * proper mime type for a single file is important due to different view possibilities in other receiving apps
         * multiple files will always be sent as documents since their mime type is unknown
         * */
        intentShareFile.setType(mimeType);
        startActivity(Intent.createChooser(intentShareFile, "Share Files"));
        return true;
    }

    public void performAction(final String addition)//passing string for renaming
    {
        CustDialog dataFragment = (CustDialog) getSupportFragmentManager().findFragmentByTag("action_going");
        // create the fragment and data the first time
        if (dataFragment == null) {
            // add the fragment
            dataFragment = CustDialog.newInstance(CustDialog.await, "", null);
            dataFragment.setRetainInstance(true);
            getSupportFragmentManager().beginTransaction().add(dataFragment, "action_going").commit();
            getSupportFragmentManager().executePendingTransactions();
        }
        //dataFragment.show(getSupportFragmentManager(), "action_going");
        final Handler action_complete = new Handler(msg -> {
            if(msg.what==1) {
                if((MainActivity.this.last_action & MainActivity.copy) == MainActivity.copy)
                {
                    toolbar.getMenu().findItem(R.id.clear_copied).setVisible(true);
                    toolbar.getMenu().findItem(R.id.paste).setVisible(true);
                }
                updateForCurrDir(((LinearLayoutManager) MainActivity.this.rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition());
                controlSelected(new boolean[MainActivity.this.getApp().fileHandler.getContents().size()]);
                Toast.makeText(MainActivity.this, getString(R.string.action) + getString(R.string.action_succeeded), Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(MainActivity.this, getString(R.string.action) + getString(R.string.action_failed), Toast.LENGTH_SHORT).show();
            }
            CustDialog progress = ((CustDialog)getSupportFragmentManager().findFragmentByTag("action_going"));
            if(progress != null)
                progress.dismiss();
            return msg.what==1;
        });
        Thread act_handle = new Thread(() -> {
            boolean success = false;
            FileSurfApp app = (FileSurfApp)getApplication();
            switch (MainActivity.this.last_action)
            {
                case MainActivity.copy:
                case MainActivity.del|MainActivity.copy:
                    success = app.fileHandler.fileCopy(app.fileHandler.filesByNums(picked), (MainActivity.this.last_action & MainActivity.del) !=0);//a sly way to determine cutting or copying
                    break;

                case MainActivity.del:
                    String[] delFiles = app.fileHandler.filesByNums(picked);
                    success = app.fileHandler.fileDel(delFiles);
                    removeBookmark(delFiles);
                    break;
                case MainActivity.rename:
                    success = app.fileHandler.rename(app.fileHandler.findByNum(picked.get(0)),addition);
                    break;
                case MainActivity.paste:
                    success = app.fileHandler.paste();
                    break;
                case MainActivity.add:
                case MainActivity.add_dir:
                    boolean dir = (MainActivity.this.last_action ^ MainActivity.add)!=0;//find out if adding a file or a dir
                    success = app.fileHandler.add(dir,addition);
                    break;
                case MainActivity.share:
                    success = shareFiles(app.fileHandler.filesByNums(picked));
                    break;
                case MainActivity.zip:
                    success = app.fileHandler.archiveFiles(app.fileHandler.filesByNums(picked));
                    break;
                default:
                    if(addition != null) {
                        success = app.fileHandler.getFileCallback(addition).performAction(app.fileHandler.filesByNums(picked));
                    }
                    break;
            }
            action_complete.sendEmptyMessage(success ? 1:0);//bool to int cast
        });
        act_handle.start();
    }

    public void openPublicLink(View button)
    {
        Uri webpage = Uri.parse("https://".concat((String)button.getTag()));
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

}
