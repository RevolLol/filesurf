package com.gmail.eurevollol.filesurf;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class bmManager {
    private JSONObject bookmarks;

    public bmManager(){
        try {
            bookmarks = new JSONObject("{}");
        }
        catch(JSONException exp)
        {
            Log.d("bmMangerFail", exp.getLocalizedMessage());
        }
    }

    public bmManager(String jsonBms){
        try {
            bookmarks = new JSONObject(jsonBms);
        }
        catch(JSONException exp)
        {
            Log.d("bmMangerFail", exp.getLocalizedMessage());
        }
    }

    public bmManager addBm(String bmKey) throws JSONException{
        bookmarks.put(bmKey, bmKey.substring(bmKey.lastIndexOf("/")));
        return this;
    }

    public bmManager addBm(String bmKey, String bmName) throws JSONException{
        bookmarks.put(bmKey, bmName);
        return this;
    }

    public bmManager removeBm(String bmKey){
        bookmarks.remove(bmKey);
        return this;
    }

    //key is path
    public boolean isBooked(String bmKey){
        return this.bookmarks.has(bmKey);
    }

    public Map getBMap(){
        Map<String, String> bMap = new HashMap<>();
        Iterator<String> bmIterator = this.bookmarks.keys();
        try{
            while(bmIterator.hasNext()){
                String nextKey = bmIterator.next();
                bMap.put(nextKey, this.bookmarks.getString(nextKey));
            }
        }
        catch (JSONException ignored){}
        return bMap;
    }

    public String toString(){
        return this.bookmarks.toString();
    }
}
