package com.gmail.eurevollol.filesurf.callbacks;

import com.gmail.eurevollol.filesurf.CustDialog;

public interface dlgActionCallback {
    void performAction(CustDialog dialog);
}
