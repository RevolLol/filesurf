package com.gmail.eurevollol.filesurf;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder>{
    private static ClickListener clickListener;
    private List<File> ent;
    private boolean[] selected;
    private byte sel_size = 0;
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public ImageView fl_Icon;
        public TextView fl_name;
        public TextView fl_properties;
        public CheckBox fl_checked;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
            fl_Icon = v.findViewById(R.id.fl_Icon);
            fl_name = v.findViewById(R.id.fl_name);
            fl_properties = v.findViewById(R.id.fl_properties);
            fl_checked = v.findViewById(R.id.fl_checked);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return true;
        }
    }

    public void set_list(List<File> files)
    {
        this.ent=files;
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        FileAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }

    public FileAdapter(List<File> files)
    {
        set_list(files);
        setSelected(new boolean[files.size()]);
    }

    public boolean[] getSelected()
    {
        return this.selected;
    }

    public void setSelected(boolean[] sel)
    {
        this.selected = sel;
        this.sel_size = 0;
        for(boolean checked:this.selected)
        {
            sel_size += checked ? 1:0;//counting the number of selected checkboxes, avoiding saving it in savedinstancestate and restoring it later
        }
    }

    public byte getSel_size()
    {
        return this.sel_size;
    }

    public String get_size(double size)//file size in bytes
    {
        int i = 0;
        String[] units = new String[]{" B", " Kb", " Mb", " Gb", " Tb"};
        while(size>=1024) {
            size/=1024;
            i++;
        }
        return new DecimalFormat("#.##").format(size)+units[i];
    }

    @Override
    public FileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fileinfo, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final FileAdapter.ViewHolder holder, final int position) {
        String f_name = ent.get(position).getName();

        holder.fl_name.setText(f_name);
        holder.fl_properties.setText(new SimpleDateFormat("dd-MM-yy HH:mm").format(new Date(ent.get(position).lastModified())));//setting creation date
        holder.fl_checked.setOnCheckedChangeListener(null);//avoiding some random errors
        holder.fl_checked.setVisibility(View.VISIBLE);//all checkboxes are visible by default
        holder.fl_checked.setChecked(this.selected[position]);//taking checkbox status from selected array

        holder.fl_checked.setOnCheckedChangeListener((buttonView, isChecked) -> {
            selected[holder.getAdapterPosition()] = isChecked;//saving checkbox status to selected array
            sel_size += isChecked ? 1:-1;
            ((MainActivity)holder.fl_checked.getContext()).onCheckHandle(sel_size);
        });

        if(ent.get(position).isDirectory()) {
            holder.fl_Icon.setImageResource(R.drawable.ic_folder_icon);
            if(f_name.equals(".."))//if we're dealing with parent directory (i.e. "..")
            {
                holder.fl_properties.setText(holder.fl_properties.getContext().getString(R.string.parent_dir));
                holder.fl_checked.setVisibility(View.GONE);
                holder.fl_checked.setOnCheckedChangeListener(null);
            }
        }
        else {
            int i = f_name.lastIndexOf('.');
            String extension = i >= 0 ? f_name.substring(i+1) : "";
            //get identifier returns zero if res wasn't found
            int icon_id = holder.fl_Icon
                    .getContext()
                    .getResources().getIdentifier("ic_file_" + extension, "drawable", holder.fl_Icon.getContext().getPackageName());
            holder.fl_properties.append("; ".concat(this.get_size((double)ent.get(position).length())));
            if(icon_id != 0)
            {
                //picasso will either load an image and fit it into the icon or put a placeholder for other non-viewable files
                Picasso.get().load(ent.get(position)).placeholder(icon_id)
                        .fit()
                        .centerCrop()
                        .into(holder.fl_Icon);
            }
            else
            {
                holder.fl_Icon.setImageResource(R.drawable.ic_file_icon);
            }
        }
    }

    @Override
    public int getItemCount() {
        return ent.size();
    }
}
