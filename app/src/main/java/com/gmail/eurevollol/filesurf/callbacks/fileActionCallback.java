package com.gmail.eurevollol.filesurf.callbacks;

public interface fileActionCallback {
    boolean performAction(String[] files);
}
