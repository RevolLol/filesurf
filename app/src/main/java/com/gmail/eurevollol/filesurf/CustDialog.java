package com.gmail.eurevollol.filesurf;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gmail.eurevollol.filesurf.callbacks.dlgActionCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Eugene on 08.05.2016.
 */
public class CustDialog extends DialogFragment implements OnClickListener, View.OnClickListener {
    public static final byte await = -1, action_menu = 0, about = 1, confirm = 2, ren_text = 8, add = 16;

    private Map<String, Object> resDlgBundle = new HashMap<>();

    private dlgActionCallback callback = null;

    CustDialog setActionCallback(dlgActionCallback cb){
        this.callback = cb;
        return this;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        this.callback = null;
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        //Fragment dialog had been dismissed
        if(this.callback != null) {
            this.callback.performAction(this);
        }
    }

    public Map<String, Object> getResultingBundle() {
        return this.resDlgBundle;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        FragmentActivity activity = getActivity();
        AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View msgview;
        String addition = getArguments().getString("addition","");
        switch(getArguments().getByte("d_type"))
        {
            case CustDialog.action_menu:
                msgview = inflater.inflate(R.layout.action_menu, null);
                adb.setTitle(R.string.options).setView(msgview);
                LinearLayout btnCont = msgview.findViewById(R.id.button_container);
                ArrayList<String> specialActions = getArguments().getStringArrayList("specialActions");
                for(View button:btnCont.getTouchables())
                {
                    button.setOnClickListener(this);
                }
                if(specialActions != null) {
                    for (String action : specialActions) {
                        Button actionButton = (Button)inflater.inflate(R.layout.actionbutton, null);
                        int resId = getResources().getIdentifier(action + "Action", "string", activity.getPackageName());
                        actionButton.setTag(action);
                        actionButton.setText(getString(resId));
                        actionButton.setOnClickListener(this);
                        btnCont.addView(actionButton);
                    }
                }
                setStyle(STYLE_NORMAL,0);
                break;

            case CustDialog.about:
                View titleview = inflater.inflate(R.layout.about_title, null);
                msgview = inflater.inflate(R.layout.about_msg, null);
                ((TextView) msgview.getRootView().findViewById(R.id.msg_ver)).append(addition);
                adb.setCustomTitle(titleview).setNegativeButton(R.string.ok, this);
                adb.setView(msgview);
                break;

            case CustDialog.confirm:
                adb.setTitle(R.string.action_confirm)
                    .setMessage(getString(R.string.delete) + addition + "?")
                    .setNegativeButton(R.string.no, this)
                    .setPositiveButton(R.string.yes, this);
                break;

            case CustDialog.ren_text:
                msgview = inflater.inflate(R.layout.action_name, null);
                ((EditText)msgview.getRootView().findViewById(R.id.new_name)).setText(addition);
                adb.setTitle(R.string.rename).setPositiveButton(R.string.ok,this).setNegativeButton(R.string.no, this).setView(msgview);
                break;

            case CustDialog.add:
                msgview = inflater.inflate(R.layout.action_name, null);
                ((EditText)msgview.getRootView().findViewById(R.id.new_name)).setText(addition);
                adb.setTitle(R.string.new_file).setPositiveButton(R.string.ok,this).setNegativeButton(R.string.no, this).setView(msgview);
                break;

            case CustDialog.await:
                msgview = inflater.inflate(R.layout.action_going, null);
                adb.setTitle(R.string.action_going)
                        .setView(msgview);
                setCancelable(true);
                break;
        }
        return adb.create();
    }

    public static CustDialog newInstance(Byte d_type, String addition, dlgActionCallback callback) {
        CustDialog f = new CustDialog().setActionCallback(callback);
        Bundle args = new Bundle();
        args.putByte("d_type", d_type);
        args.putString("addition", addition);
        f.setArguments(args);
        return f;
    }

    public static CustDialog newInstance(ArrayList<String> specialActions, String addition, dlgActionCallback callback) {
        CustDialog newDialog = CustDialog.newInstance(CustDialog.action_menu, addition, callback);
        Bundle args = newDialog.getArguments();
        args.putStringArrayList("specialActions", specialActions);
        newDialog.setArguments(args);
        return newDialog;
    }


    public void onClick(DialogInterface dialog, int which) {
        if(which != Dialog.BUTTON_NEUTRAL && which != Dialog.BUTTON_POSITIVE)
        {
            this.callback = null;
        }
        dismiss();
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        //we just need id to be anything but -1, cuz the button was indeed pressed, but it's dynamic
        this.resDlgBundle.put("selectedAction", btnId != -1 ? btnId : 0);
        this.resDlgBundle.put("specialAction", v.getTag());
        this.resDlgBundle.put("actionName", ((Button)v).getText().toString());
        dismiss();
    }
}
