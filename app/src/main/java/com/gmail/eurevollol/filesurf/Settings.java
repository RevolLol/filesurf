package com.gmail.eurevollol.filesurf;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class Settings extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, AccessesApp{
    private SharedPreferences pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        pref = getSharedPreferences("FS_set", MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        RelativeLayout ml = findViewById(R.id.mainLayout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//back arrow in actionbar
        getSupportActionBar().setDisplayShowHomeEnabled(true);//same here
        ArrayList<View> allViews = getAllChildren(ml);
        //setting checkboxes according to previously saved preferences
        for (View child : allViews) {
            if (child instanceof CheckBox) {
                String viewname = getResources().getResourceEntryName(child.getId());
                CheckBox childCB = (CheckBox) child;
                childCB.setChecked(pref.getBoolean(viewname, false));
                childCB.setOnCheckedChangeListener(this);
            }
        }
        findViewById(R.id.theme).setEnabled(!pref.getBoolean("theme_global", false));
        /*CheckBox theme = (CheckBox)findViewById(R.id.theme);
        CheckBox pos = (CheckBox)findViewById(R.id.position);
        theme.setOnCheckedChangeListener(this);
        pos.setOnCheckedChangeListener(this);
        theme.setChecked(pref.getBoolean("theme", false));
        pos.setChecked(pref.getBoolean("position", false));*/

    }

    /*@Override
    protected  void onDestroy()
    {
        ml = (RelativeLayout)findViewById(R.id.mainLayout);
        ArrayList<View> allViews = getAllChildren(ml);//to another thread
        for (View child : allViews) {
            if (child instanceof CheckBox) {
                String viewname = getResources().getResourceName(child.getId());
                CheckBox childCB = (CheckBox) child;
                saveSet(viewname, childCB.isChecked());//key is view name(id)
            }
        }
        super.onDestroy();
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:// app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private ArrayList<View> getAllChildren(View v) {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup vg = (ViewGroup) v;
        for (int i = 0; i < vg.getChildCount(); i++) {

            View child = vg.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(getAllChildren(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    private void saveSet(String key, boolean val)
    {
        pref = getSharedPreferences("FS_set", MODE_PRIVATE);
        SharedPreferences.Editor ed = pref.edit();
        ed.putBoolean(key, val);
        ed.apply();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int checkId = buttonView.getId();
        String prefKey = getResources().getResourceEntryName(checkId);
        saveSet(prefKey,buttonView.isChecked());
        //specific processing
        switch (checkId){
            case R.id.theme:
                getApp().setTheme();
                recreate();
                break;
            case R.id.theme_global:
                findViewById(R.id.theme).setEnabled(!isChecked);
                getApp().setTheme();
                recreate();
                break;
        }
    }

    @Override
    public FileSurfApp getApp() {
        return (FileSurfApp)getApplication();
    }
}
