package com.gmail.eurevollol.filesurf;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.StatFs;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.os.EnvironmentCompat;

import com.gmail.eurevollol.filesurf.callbacks.fileActionCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Entries implements Parcelable {

    private static class Sorters{
        private static int nameCompare(File o1, File o2){
            String name1 = o1.getName(),
                    name2 = o2.getName();
            int num1 = extractInt(name1),
                    num2 = extractInt(name2);
            //checking if both names contain numbers and
            if(num1 * num2 != 0 && name1.replace("\\d*$", "").compareToIgnoreCase(name2.replace("\\d*$", "")) == 0)
            {
                return num1 - num2;
            }
            return name1.compareToIgnoreCase(name2);
        }

        private static int sizeCompare(File o1, File o2) {
            return Sorters.castToInt(o1.length() - o2.length());
        }

        private static int castToInt(long val){
            return (int) Math.max(Math.min(Integer.MAX_VALUE, val), Integer.MIN_VALUE);
        }

        private static int lastModCompare(File o1, File o2){
            return Sorters.castToInt(o1.lastModified() - o2.lastModified());
        }

        private static int extractInt(String s) {
            String num = s.replaceAll("^\\D*", "");
            // return 0 if no digits found
            return num.isEmpty() ? 0 : Integer.parseInt(num);
        }
    }

    public static Map<String, List<String>> extensionActions = new HashMap<>();
    static {
        extensionActions.put("zip", new ArrayList<>(Arrays.asList("unzip")));
    }

    public static String getAbsoluteDir(Context ctx, String optionalPath) {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            return Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        String rootPath;
        if (optionalPath != null && !optionalPath.equals("")) {
            rootPath = ctx.getExternalFilesDir(optionalPath).getAbsolutePath();
        } else {
            rootPath = ctx.getExternalFilesDir(null).getAbsolutePath();
        }
        // extraPortion is extra part of file path
        String extraPortion = "Android/data/" + BuildConfig.APPLICATION_ID + File.separator + "files";
        // Remove extraPortion
        return rootPath.replace(extraPortion, "").replaceFirst("\\/$", "");
    }

    private File curr_d;
    private List<File> contents;
    private boolean cut = false;
    private String[] copy_queue;
    private List<String> sdCardPaths;
    private String root;
    private Context app_cont;

    public Entries(Context context)//default constructor with /storage/emulated/0 initial path
    {
        this.app_cont = context.getApplicationContext();
        this.init("");
    }

    public Entries(String start, Context context)//this constructor allows to init custom initial path
    {
        this.app_cont = context.getApplicationContext();
        this.init(start);
    }

    public fileActionCallback getFileCallback(String name){
        return (String... files) -> {
            try {
                return (boolean)Entries.class.getDeclaredMethod(name, String[].class).invoke(this, new Object[]{files});
                //return (boolean) Entries.class.getMethod(name, String[].class).invoke(this, files);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                return false;
            }
        };
    }

    private void init(String init_path)
    {
        this.sdCardPaths = Entries.getSdCardPaths(this.app_cont,false);
        /**
         * trying to figure out what's the root for the initial directory
         * checking if we have any sd card available and the initial path contains (or equals) the sd card root
         * otherwise external storage is considered root
         * */
        if(this.sdCardPaths != null && init_path.contains(this.sdCardPaths.get(0)))
        {
            this.root = this.sdCardPaths.get(0);
        }
        else
        {
            this.root = Entries.getAbsoluteDir(this.app_cont, "");
            //Environment.getExternalStorageDirectory().getAbsolutePath()
        }
        this.curr_d = new File(init_path).exists() ? new File(init_path) : new File(this.root);
        this.contents = new ArrayList<>();
        List <File> temp = new ArrayList<>(Arrays.asList(this.curr_d.listFiles()));
        //sort files case-insensitively respecting possible numbers in the end of string
        Collections.sort(temp, (o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
        if(this.goUpper())
            this.contents.add(new File(init_path.concat("/..")));
        this.contents.addAll(temp);
    }

    public File findByNum(int number)
    {
        return this.contents.get(number);
    }

    public String[] filesByNums(ArrayList<Integer> nums)
    {
        String [] files = new String[nums.size()];
        for (int i = 0; i<nums.size(); i++) {
            try
            {
                files[i] = this.findByNum(nums.get(i)).getCanonicalPath();
            }
            catch(IOException exp)
            {
                return new String[]{""};
            }
        }
        return files;
    }

    public Entries changeDir(String path)
    {
        this.init(path);
        return this;
    }

    public String getRootFor(boolean sdCard){
        return sdCard ? this.sdCardPaths.get(0) : getAbsoluteDir(app_cont, null);
    }

    public boolean getIsSDCard(){
        return !this.root.equals(getAbsoluteDir(app_cont, null));
    }

    public File getCurrDir()
    {
        return this.curr_d;
    }

    public String getCurrPath()
    {
        try
        {
            return this.goUpper() ? this.curr_d.getCanonicalPath().replace(this.root, ""): "/";
        }
        catch (IOException nofile)
        {
            return  getAbsoluteDir(app_cont, null);
        }

    }

    public boolean goUpper()
    {
        try {
            return !this.curr_d.getCanonicalPath().equals(this.root);
        }
        catch (IOException nofile)
        {
            return false;
        }
    }

    public List<File> getContents()
    {
        return this.contents;
    }

    public String getCanonPath(File file)
    {
        try
        {
            return file.getCanonicalPath();
        }
        catch(IOException exp)
        {
            return file.getAbsolutePath();
        }
    }

    public List<String> getSdCardPaths() {
        return sdCardPaths;
    }

    public static List<String> getSdCardPaths(final Context context, final boolean includePrimaryExternalStorage)
    {
        final File[] externalCacheDirs= ContextCompat.getExternalCacheDirs(context);
        if(externalCacheDirs==null||externalCacheDirs.length==0)
            return null;
        if(externalCacheDirs.length==1)
        {
            if(externalCacheDirs[0]==null)
                return null;
            final String storageState= EnvironmentCompat.getStorageState(externalCacheDirs[0]);
            if(!Environment.MEDIA_MOUNTED.equals(storageState))
                return null;
            if(!includePrimaryExternalStorage && Environment.isExternalStorageEmulated())
                return null;
        }
        final List<String> result=new ArrayList<>();
        if(includePrimaryExternalStorage||externalCacheDirs.length==1)
            result.add(getRootOfInnerSdCardFolder(externalCacheDirs[0]));
        for(int i=1;i<externalCacheDirs.length;++i)
        {
            final File file=externalCacheDirs[i];
            if(file==null)
                continue;
            final String storageState=EnvironmentCompat.getStorageState(file);
            if(Environment.MEDIA_MOUNTED.equals(storageState))
                result.add(getRootOfInnerSdCardFolder(externalCacheDirs[i]));
        }
        if(result.isEmpty())
            return null;
        return result;
    }

    /** Given any file/folder inside an sd card, this will return the path of the sd card */
    private static String getRootOfInnerSdCardFolder(File file)
    {
        if(file==null)
            return null;
        final long totalSpace=file.getTotalSpace();
        while(true)
        {
            final File parentFile=file.getParentFile();
            if(parentFile==null||parentFile.getTotalSpace()!=totalSpace)
                return file.getAbsolutePath();
            file=parentFile;
        }
    }

    public boolean isQueueEmpty()
    {
        return this.copy_queue == null;
    }

    public void cleanQueue()
    {
        this.copy_queue = null;
    }

    private String getUniqueName(File to){
        String newName = to.getName();
        int j = newName.lastIndexOf(".") >= 0 ? newName.lastIndexOf(".") : newName.length();
        for(int i = 1; to.exists(); i++)
        {
            to = new File(this.getCanonPath(this.curr_d), new StringBuilder(newName).insert(j," ("+i+")").toString());
        }
        return to.getName();
    }

    public boolean rename(File from, String newName)
    {
        File to = new File(this.getCanonPath(this.curr_d), getUniqueName(new File(this.getCanonPath(this.curr_d), newName)));
        boolean temp_bool = from.renameTo(to);
        if(temp_bool) {
            MediaScannerConnection.scanFile(this.app_cont, new String[]{from.getAbsolutePath(), to.getAbsolutePath()}, null, null);//updating system file index
        }
        return temp_bool;
    }

    public boolean paste()
    {
        String curr_dir;
        try {
            curr_dir = this.curr_d.getCanonicalPath();
        }
        catch (IOException exp) {
            curr_dir = this.curr_d.getAbsolutePath();
        }
        return pasteHelper(this.copy_queue,curr_dir);
    }

    private boolean pasteHelper(String [] ent, String path)
    {
        String sh = "sh";
        String c = "-c";
        String cat_command = "cat \"$1\" > \"$2\"";
        boolean die = false;//flag of operation went wrong; is used to break out of possible recursion
        for (String file:ent)//looping through given file paths
        {
            File temp_file_orig = new File(file);//original file from the queue
            String new_path = path + "/" + getUniqueName(new File(path + "/" + temp_file_orig.getName()));//creating new path for files
            if(temp_file_orig.isDirectory())
            {
                File new_dir = new File(new_path);
                new_dir.mkdir();
                File[] temp_arr = temp_file_orig.listFiles();
                String[] paths = new String[temp_arr.length];
                for (int j = 0; j < temp_arr.length; j++) {
                    paths[j] = temp_arr[j].getAbsolutePath();
                }
                die = this.pasteHelper(paths, new_path);
            }
            else
                if(!die) {
                    try {
                        Runtime.getRuntime().exec(new String[]{
                                sh, c, cat_command, "--", file, new_path
                        }).waitFor();//waiting for process to finish, then continue
                    } catch (IOException | InterruptedException exp) {
                        return true;//stop the execution when true
                    }
                    MediaScannerConnection.scanFile(this.app_cont, new String[]{new_path}, null, null);//updating system file index
                }
                else
                {
                    return true;
                }
        }
        return this.cut ? fileDel(ent) && !die : !die;
    }

    public boolean fileCopy(String[] to_copy, boolean cut)
    {
        this.cut = cut;
        this.copy_queue = to_copy;
        return true;
    }


    public boolean fileDel(String[] to_del) {
        boolean die = false;//flag of delete that went wrong to break out of possible recursion
        for(String f_path:to_del)
        {
            File temp_file = new File(f_path);
            if(temp_file.isDirectory())
            {
                File[] temp_arr = temp_file.listFiles();
                String[] paths = new String[temp_arr.length];
                for (int i = 0; i < temp_arr.length; i++) {
                    paths[i] = temp_arr[i].getAbsolutePath();
                }
                die = !fileDel(paths);
            }
            if(!temp_file.delete() || die)
            {
                return true;//returning from current function call
            }
        }
        MediaScannerConnection.scanFile(this.app_cont, to_del, null, null);//updating system file index
        return !die;
    }

    public boolean add(boolean isDir, String fileName)//obviously a function for adding files and folders
    {
        String curr_dir;
        boolean success = false;
        try {
            curr_dir = this.curr_d.getCanonicalPath();
        }
        catch (IOException exp) {
            curr_dir = this.curr_d.getAbsolutePath();
        }
        String new_path = curr_dir + "/" + fileName;
        File temp_f = new File(new_path);
        if(isDir)
            success = temp_f.mkdir();
        else {
            try {
                success = temp_f.createNewFile();
            }
            catch (IOException exp)
            {
                success = false;
            }
        }
        if(success)
            MediaScannerConnection.scanFile(this.app_cont, new String[]{new_path}, null, null);//updating system file index
        return success;
    }

    private void getMetadata(){
    }

    public double getTotalMemory()
    {
        StatFs statFs = new StatFs(this.root);
        return (double) (statFs.getBlockCountLong() * statFs.getBlockSizeLong());
    }

    public double getFreeMemory()
    {
        StatFs statFs = new StatFs(this.root);
        return (double)(statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong());
    }

    public double getBusyMemory()
    {
        return this.getTotalMemory() - this.getFreeMemory();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.curr_d);
        dest.writeList(this.contents);
        dest.writeByte(this.cut ? (byte) 1 : (byte) 0);
        dest.writeStringArray(this.copy_queue);
        dest.writeString(this.root);
    }

    protected Entries(Parcel in) {
        this.curr_d = (File) in.readSerializable();
        this.contents = new ArrayList<>();
        in.readList(this.contents, File.class.getClassLoader());
        this.cut = in.readByte() != 0;
        this.copy_queue = in.createStringArray();
        this.root = in.readString();
    }

    public static final Parcelable.Creator<Entries> CREATOR = new Parcelable.Creator<Entries>() {
        @Override
        public Entries createFromParcel(Parcel source) {
            return new Entries(source);
        }

        @Override
        public Entries[] newArray(int size) {
            return new Entries[size];
        }
    };

    private boolean unzip(String[] files){
        String archiveFile = files[0],
                dirPath = archiveFile.substring(0, archiveFile.lastIndexOf("/") + 1),
                //constructing a new unique extract directory name
                pathToSave = dirPath + getUniqueName(new File(dirPath + archiveFile.replace(dirPath, "").split("\\.")[0])) + "/";
        try {
            new File(pathToSave).mkdir();
            final ZipFile zipFile = new ZipFile(archiveFile);
            final Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while ( entries.hasMoreElements() ) {
                final ZipEntry nextEntry = entries.nextElement();
                InputStream inStream = zipFile.getInputStream(nextEntry);
                String entryName = nextEntry.getName(), addPath;
                /**
                 * all files including nested are located on the same level in archive,
                 * so we have to check for that and create subdirs when necessary
                 * */
                if(entryName.contains("/")){
                    addPath = entryName.substring(0, entryName.lastIndexOf("/") + 1);
                    new File(pathToSave + addPath).mkdirs();
                }

                FileOutputStream fileOut = new FileOutputStream(pathToSave + entryName);
                copyByteData(inStream, fileOut);
                fileOut.flush();
                inStream.close();
                fileOut.close();
            }
            zipFile.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public boolean archiveFiles(String[] archiveFiles){
        try{
            File[] initialFiles = new File[archiveFiles.length];
            for(int i = 0; i < archiveFiles.length; i++)
                initialFiles[i] = new File(archiveFiles[i]);
            String archiveName = initialFiles.length == 1 ? initialFiles[0].getName() : this.getCurrDir().getName();
            ZipOutputStream newZip = new ZipOutputStream(new FileOutputStream(this.getCurrDir().getAbsolutePath() + "/".concat(archiveName).concat(".zip")));
            newZip.setLevel(Deflater.DEFAULT_STRATEGY);
            doZip(initialFiles, newZip, "");
            newZip.close();
            return true;
        }
        catch (IOException exp)
        {
            return false;
        }
    }

    private void doZip(File[] archiveFiles, ZipOutputStream out, String parentDir) throws IOException {
        for(File archFile: archiveFiles)
        {
            if(archFile.isDirectory())
                doZip(archFile.listFiles(), out, archFile.getName().concat("/"));
            else {
                out.putNextEntry(new ZipEntry(parentDir.concat(archFile.getName())));
                FileInputStream fIn = new FileInputStream(archFile);
                copyByteData(fIn, out);
                fIn.close();
                out.closeEntry();
            }
        }
    }

    //helper method to read byte data from input streams and write it out to output streams in chunks
    private void copyByteData(InputStream inStream, OutputStream outStream) throws IOException {
        int bytesRead;
        //bufferizing chunks of data instead of writing bytes one by one
        byte[] buffer = new byte[65536];
        while((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
    }

    public ArrayList<Uri> getUrisForFiles(String[] files){
        ArrayList<Uri> fileUris = new ArrayList<>();
        for(String filePath: files)
        {
            fileUris.add(FileProvider.getUriForFile(this.app_cont, BuildConfig.APPLICATION_ID + ".provider", new File(filePath)));
        }
        return fileUris;
    }
}
