# FileSurf application

A not that unique looking material designed file manager for Android 5.0+


### Installing

No prebuilt APK files are included, so you'll have to build and run it yourself

## Built With

* [Gradle](https://gradle.org/) - Build system & dependency management
* [Picasso](https://square.github.io/picasso/) - Image processing framework used for creating file lists
* [MaterialSheetFab](https://github.com/gowong/material-sheet-fab) - a small library for making FAB menu

## Authors

* **Eugene Kalashnikov** - *Literally everything except external libs stated above* - [RevolLol](https://bitbucket.org/revollol)

## License

IDK tho. Remind me to check that later

## Acknowledgments
* Huge thanks to hundreds and hundreds of StackOverflow developers for code snippets
* A hat tip goes to my friend Alexander who initially dragged me into Android development